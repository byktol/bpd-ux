// ==UserScript==
// @name       BPD-UX
// @namespace  http://byktol.com/
// @version    0.1
// @description  A small script that allows the user to press the 'Enter' key to sign-in @ BPD (Banco Popular Dominicano).
// @match      https://www.bpd.com.do/banco.popular.aspx?*
// @copyright  2013+, Víctor Álvarez
// ==/UserScript==

if (document.querySelector('form[name=frmLogin]') !== null) {

    var submitAnchor = document.querySelector('.md-button-generic a');

    var submitButton = document.createElement('button');
    submitButton.id = 'custom-bpd-submit';
    submitButton.setAttribute('type', 'submit');

    // copy attributes
    submitButton.textContent = submitAnchor.textContent;
    submitButton.onclick = submitAnchor.onclick;

    submitAnchor.parentNode.replaceChild(submitButton, submitAnchor);
    
    // style according to what I know
    var styles = new Array(
        {
            name  : '#custom-bpd-submit',
            value : 'border: 0 none; background-color: transparent; color: white;'
        },
        {
            name  : '#custom-bpd-submit:hover',
            value : 'color: #eee !important; text-decoration:underline;'
        }
    );
    var css = document.styleSheets[0];
    if (typeof css.addRule === 'function') {
        css.addRule(styles[0].name, styles[0].value);
        css.addRule(styles[1].name, styles[1].value);

    } else if (typeof css.insertRule === 'function') {
        css.insertRule(styles[0].name + '{' + styles[0].value + '}', css.cssRules.length);
        css.insertRule(styles[1].name + '{' + styles[1].value + '}', css.cssRules.length);
    }
}